from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


# if o is an instance of a QuerySet, just turn it into a normal list rather than the fancy list that it already is.
class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # https://learn-2.galvanize.com/cohorts/3189/blocks/1873/content_files/build/02-more-ddd/71-building-your-json-library.md
    # this is added as part of "if property in self.encoders" code block below
    # Without this line, an AttributeError is thrown
    # When Show conference detail HTTP GET request is sent, the error is:
    # "'LocationListEncoder' object has no attribute 'encoders'"
    encoders = {}

    def default(self, o):
        # if the object to decode is the same class as what's in the model property, then
        if isinstance(o, self.model):
            # create an empty dictionary that will hold the property names
            # as keys and the property values as values
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
                # then add its return value to the dictionary with the key "href"
                d["href"] = o.get_api_url()

            # for each name in the properties list
            for property in self.properties:
                # get the value of that property from the model instance given just the property name
                value = getattr(o, property)

                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)

                # put it into the dictionary with that property name as the key
                d[property] = value

            d.update(self.get_extra_data(o))

            # return the dictionary
            return d
        # otherwise,
        else:
            # return super().default(o)  # From the documentation
            return super().default(o)

    def get_extra_data(self, o):
        return {}
