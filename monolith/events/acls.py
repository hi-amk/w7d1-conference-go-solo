from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_picture_url(city):
    url = "https://api.pexels.com/v1/search?"
    payload = {"query": city, "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}

    try:
        response = requests.get(url, params=payload, headers=headers)
        response_python = json.loads(response.content)
        # print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", response_python)

        picture_url = response_python["photos"][0]["src"]["original"]

        return {"picture_url": picture_url}
    # (when the city is invalid, response_python["photos"] is an empty list)
    # (so tryint to access index 0 of the list by response_python["photos"][0] will throw an IndexError)
    except IndexError:
        return {"picture_url": None}


def get_weather_info(city, state):
    # print(city, state)
    geocoding_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"

    try:
        geocoding_response = requests.get(geocoding_url)
        geocoding_response_python = json.loads(geocoding_response.content)

        latitude = geocoding_response_python[0]["lat"]
        longitude = geocoding_response_python[0]["lon"]
    # (when the city and state are invalid, geocoding_search_content is an empty list)
    # (so tryint to access index 0 of the list by geocoding_search_content[0] will throw an IndexError)
    except IndexError:
        return None

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units=imperial"

    try:
        weather_response = requests.get(weather_url)
        weather_response_python = json.loads(weather_response.content)

        temperature = weather_response_python["main"]["temp"]
        descrption = weather_response_python["weather"][0]["description"]

        return {
            "temp": temperature,
            "description": descrption,
        }
    except KeyError:
        return None
