# https://learn-2.galvanize.com/cohorts/3189/blocks/1873/content_files/build/05-hello-docker/66-dockerize-the-app.md
# This is the DEPLOYMENT dockerfile for the Conference GO application, to be built and run (after creating volume) by:
# <<docker build . -t conference-go>>
# <<docker run -p 8000:8000 -v conference-go-db:/app/data conference-go>>

# Select the base image that is best for our application
FROM python:3

# Install any operating system junk


# Set the working directory to copy stuff to
WORKDIR /app

# Copy all the code from the local directory into the image
COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py

# Install any language dependencies
RUN pip install -r requirements.txt


# Set the command to run the application
# CMD gunicorn conference_go.wsgi
# https://learn-2.galvanize.com/cohorts/3189/blocks/1873/content_files/build/05-hello-docker/66-dockerize-the-app.md
# When you run that, it seems to start okay. If you try to access it in Insomnia, though, you'll see that it fails to connect. This is due to an interesting way that gunicorn (and Django) run. This is going to take a moment to explain.
# Your Django app is now running inside another virtual computer
# It starts up and reports Listening at: http://127.0.0.1:8000
# The application is only listening to the local computer inside the virtual computer
# We're making a request with Insomnia from a whole different computer!
# Because of this, gunicorn can't hear our request.
# To fix this, we change the Dockerfile to tell gunicorn to listen to all the requests. Replace the CMD in your file with this one.
CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi