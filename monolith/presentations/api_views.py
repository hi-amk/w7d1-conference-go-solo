from django.http import JsonResponse
from pkg_resources import require

from events.api_views import ConferenceListEncoder

from .models import Presentation, Status
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
import pika


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})

    if request.method == "GET":
        presentations = Presentation.objects.filter(
            conference_id=conference_id
        ).all()
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)
        content["status"] = Status.objects.get(name="SUBMITTED")
        content["conference"] = Conference.objects.get(id=conference_id)

        new_presentation = Presentation.objects.create(**content)
        return JsonResponse(
            new_presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, pk):
    """
    Returns the details for the Presentation model specified
    by the pk parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # presentation = Presentation.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "presenter_name": presentation.presenter_name,
    #         "company_name": presentation.company_name,
    #         "presenter_email": presentation.presenter_email,
    #         "title": presentation.title,
    #         "synopsis": presentation.synopsis,
    #         "created": presentation.created,
    #         "status": presentation.status.name,
    #         "conference": {
    #             "name": presentation.conference.name,
    #             "href": presentation.conference.get_api_url(),
    #         },
    #     }
    # )

    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        if "conference" in content:
            try:
                content["conference"] = Conference.objects.get(
                    id=content["conference"]
                )
            except Conference.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Conference ID"},
                    status=400,
                )
        # (Reminder: We're avoiding DIRECTLY accessing a Status object by "Status.objects..." because Presentation and Status are an aggregate)
        # (so we can ONLY access Status objects/instances via the Presentation model)
        # (Below code is trying to store the specific Status object in status variable. BUT it raises AttributeError: 'QuerySet' object has no attribute 'status')
        # status = Presentation.objects.filter(id=pk).status.objects.get(name=content["status"])
        # (Below code is trying to store the specific Status object in status variable. BUT it raises AttributeError: 'Manager isn't accessible via Status instances')
        # status = Presentation.objects.get(id=pk).status.objects.get(name=content["status"])
        # (Below code is trying to store the specific Status object in status variable. BUT it raises AttributeError: ''Status' objects has no attribute 'get'')
        # status = Presentation.objects.get(id=pk).status.get(name=content["status"])
        # content["status"] = status

        if "status" in content:
            status = content["status"]
            del content["status"]
        Presentation.objects.filter(id=pk).update(**content)
        updated_presentation = Presentation.objects.get(id=pk)
        if status == "APPROVED":
            updated_presentation.approve()
        elif status == "REJECTED":
            updated_presentation.reject()
        else:
            return JsonResponse(
                {"message": "Invalid Presentation Status"},
                status=400,
            )
        return JsonResponse(
            updated_presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def send_message_to_queue_as_producer(message, queue_name):
    message_json = json.dumps(message)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=message_json,
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    queue_name = "presentation_approvals"
    send_message_to_queue_as_producer(message, queue_name)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    queue_name = "presentation_rejections"
    send_message_to_queue_as_producer(message, queue_name)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
