# https://learn-2.galvanize.com/cohorts/3189/blocks/1877/content_files/build/02-queues/68-unmonolith-with-queues.md
DEBUG = True
ALLOWED_HOSTS = []
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

EMAIL_HOST = "mail"
EMAIL_SUBJECT_PREFIX = ""
