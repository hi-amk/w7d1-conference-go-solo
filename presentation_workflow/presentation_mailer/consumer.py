# https://learn-2.galvanize.com/cohorts/3189/blocks/1877/content_files/build/02-queues/68-unmonolith-with-queues.md

import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    body_python = json.loads(body)
    presenter_name = body_python["presenter_name"]
    presentation_title = body_python["title"]
    send_mail(
        "Your presentation has been accepted",
        f"{presenter_name}, we're happy to tell you that your presentation {presentation_title} has been accepted",
        "admin@conference.go",
        [body_python["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    body_python = json.loads(body)
    presenter_name = body_python["presenter_name"]
    presentation_title = body_python["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{presenter_name}, we're sorry to tell you that your presentation {presentation_title} has been rejected",
        "admin@conference.go",
        [body_python["presenter_email"]],
        fail_silently=False,
    )


print("IM WORKING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
