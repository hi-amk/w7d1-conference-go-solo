# https://learn-2.galvanize.com/cohorts/3189/blocks/1877/content_files/build/01-microservices/68-unmonolith-a-service.md

import json
import requests

from .models import ConferenceVO


def get_conference():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
