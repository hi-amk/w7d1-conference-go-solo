from django.http import JsonResponse


from .models import Attendee
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

# from events.models import Conference
from .models import ConferenceVO

# from events.api_views import ConferenceListEncoder


class ConferenceVOListEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # response = []
    # attendees = Attendee.objects.all()
    # for attendee in attendees:
    #     response.append(
    #         {"name": attendee.name, "href": attendee.get_api_url()}
    #     )
    # return JsonResponse({"attendees": response})
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            # https://learn-2.galvanize.com/cohorts/3189/blocks/1877/content_files/build/01-microservices/67-unmonolith-a-service.md
            # before splitting the monolith, we expected to get the conference ID for the conference
            # but after splitting the monolith, we can't find the right ConferenceVO object based on the Conference ID,
            # because there is no guarantee that the Conference ID will be the same as the ConferenceVO id.
            # so instead, we have to find it by the href of the Conference obj, which we have also stored as an attribute within the ConferenceVO object as "import_href"
            # the below 1 line of code is from before splitting the monolith
            # conference_obj = ConferenceVO.objects.get(id=conference_id)

            conference_href = content["conference"]
            conference_vo_obj = ConferenceVO.objects.get(
                import_href=conference_href
            )

            content["conference"] = conference_vo_obj
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID"},
                status=400,
            )
        new_attendee = Attendee.objects.create(**content)
        return JsonResponse(
            new_attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVOListEncoder(),
    }


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         },
    #     }
    # )

    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "conference" in content:
                conference_obj = ConferenceVO.objects.get(
                    id=content["conference"]
                )
                content["conference"] = conference_obj

        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID"},
                status=400,
            )

        Attendee.objects.filter(id=pk).update(**content)
        updated_attendee = Attendee.objects.get(id=pk)

        return JsonResponse(
            updated_attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
